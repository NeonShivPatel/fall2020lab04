//@author: Shiv Patel
//@ID: 1935098
package inheritance;

public class Book {
protected String bookTitle;
private String bookAuthor;

	public Book (String title, String author) {
		this.bookTitle = title;
		this.bookAuthor = author;
	}
	
	public String getTitle() {
		return this.bookTitle;
	}
	
	public String getAuthor() {
		return this.bookAuthor;
	}
	
	public String toString() {
		return ("Book Title: " + this.bookTitle + ", Book Author: " + this.bookAuthor);
	}

}

