//@author: Shiv Patel
//@ID: 1935098
package inheritance;

public class ElectronicBook extends Book {
private int numberBytes;
public ElectronicBook(String title, String author,int bytes) {
	super(title, author);
	this.numberBytes = bytes;	
}

	public String toString() {
		//Create strings containing both the parent class and sub class fields, and then returning both concatenated
		String fromBase = super.toString();
		String fromSub = ", BookMemory: " + this.numberBytes;
		return fromBase.concat(fromSub);
	}
}
