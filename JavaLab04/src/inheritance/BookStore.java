//@author: Shiv Patel
//@ID: 1935098
package inheritance;

public class BookStore extends Book {
public BookStore(String title, String author) {
	super(title, author);
}

	public static void main(String[] args) {
		Book[] library = new Book[5];
		library[0] = new Book ("Dragon Ball", "Toriyama");
		library[1] = new ElectronicBook ("Naruto", "Kishimoto", 100);
		library[2] = new Book ("Berserk", "Miura");
		library[3] = new ElectronicBook ("Bleach", "Kubo", 80);
		library[4] = new ElectronicBook ("One Piece", "Oda", 60);
		
		//Loop printing out every value in the Book[] library
		for (int i = 0; i < library.length; i++) {
			System.out.println(library[i]);
		}//end loop
	}

}
