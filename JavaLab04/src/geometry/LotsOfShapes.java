//@author: Shiv Patel
//@ID: 1935098
package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(3,5);
		shapes[1] = new Rectangle(2,7);
		shapes[2] = new Circle(3);
		shapes[3] = new Circle(8);
		shapes[4] = new Square(4);
		
		//Loop printing out every hard coded value in the Shape[] shapes
		for (int i = 0; i < shapes.length; i++) {
			System.out.println("Perimeter: " + shapes[i].getPerimeter() + ", Area: " + shapes[i].getArea());
		}//end loop
	}
}
