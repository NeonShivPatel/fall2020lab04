//@author: Shiv Patel
//@ID: 1935098
package geometry;

public class Rectangle implements Shape {
private double length;
private double height;
public Rectangle(int length, int height) {
	this.length = length;
	this.height = height;
}

	public double getLength() {
		return this.length;
	}
	
	public double getHeight() {
		return this.height;
	}

	@Override
	public double getArea() {
		return this.length*this.height;
	}

	@Override
	public double getPerimeter() {
		return (this.height*2)+(this.length*2);
	}

}
