//@author: Shiv Patel
//@ID: 1935098
package geometry;

public interface Shape {

	double getArea();
	
	double getPerimeter();
}
